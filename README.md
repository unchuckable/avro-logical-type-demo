# Example for Reference Structures on AVRO

### Approach:

* Wrap referenceable data into a Reference object that carries an ID and an optional data instance
* For the first occurrence of a data instance, store the instance along with a generated id
* For every consecutive occurrence, store only the id and leave the data field empty
* translation between data record XYZ and reference record XYZReference is performed via standard AVRO methods, if components are registered to the AVRO context (GenericData)
* Java class generation is adapted to incorporate methods for the reference translation

### Files in this Example

* /src/main/java/logical/IdlCompiler.java - generate schemata from AVDL file
* /src/main/java/logical/CustomCompiler.java - Adapted AVRO Compiler to generate java classes from Schema
* /src/main/java/logical/ReadAndWriteTest.java - test the generated example (and some nested classes)

* /src/main/references/..../record.vm - Adapted AVRO record template for specific compiler

