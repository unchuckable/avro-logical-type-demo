package logical;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.LogicalTypes;
import org.apache.avro.LogicalTypes.LogicalTypeFactory;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.generic.IndexedRecord;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonDecoder;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import logical.test.Child;
import logical.test.ChildReference;
import logical.test.Parent;

public class ReadAndWriteTest {

	private static final Logger LOGGER = LoggerFactory.getLogger( ReadAndWriteTest.class );
	
	public static class ReferenceScopeConversion extends Conversion<IndexedRecord> {

		@Override
		public Class<IndexedRecord> getConvertedType() {
			return IndexedRecord.class;
		}

		@Override
		public String getLogicalTypeName() {
			return "referenceScope";
		}
		
		@Override
		public GenericRecord fromRecord(IndexedRecord value, Schema schema, LogicalType type) {
			LOGGER.debug( "Reading a parent" );
			ReferenceScope.ENTITY_BY_ID.get().clear();
			return (Parent) value;
		}
		
		@Override
		public IndexedRecord toRecord(IndexedRecord value, Schema schema, LogicalType type) {
			LOGGER.debug("Writing a parent");
			ReferenceScope.ID_BY_ENTITY.get().clear();
			return value;
		}
	}
	
	public static class ReferenceScope extends LogicalType {

		public static ThreadLocal<IdentityHashMap<Object, Long>> ID_BY_ENTITY = ThreadLocal.withInitial( IdentityHashMap::new );
		public static ThreadLocal<HashMap<Long,Object>> ENTITY_BY_ID = ThreadLocal.withInitial( HashMap::new );
		
		public ReferenceScope() {
			super("referenceScope");
		}
		
	}

	
	public static class ReferenceConversion extends Conversion<IndexedRecord> {

		public static final ReferenceConversion INSTANCE = new ReferenceConversion();
		
		public static Map<Long,IndexedRecord> children = new HashMap<>();
		
		@Override
		public Class<IndexedRecord> getConvertedType() {
			return IndexedRecord.class;
		}

		@Override
		public String getLogicalTypeName() {
			return "reference";
		}

		@Override
		public IndexedRecord fromRecord(IndexedRecord value, Schema schema, LogicalType type) {
			LOGGER.debug( "Reading from {}", value );
			
			if ( value instanceof ChildReference ) {
				ChildReference ref = (ChildReference) value;
				if ( ref.getValue() == null ) {
					return (Child)ReferenceScope.ENTITY_BY_ID.get().get(ref.getId());
				}
				else {
					ReferenceScope.ENTITY_BY_ID.get().put( ref.getId(), ref.getValue() );
					return ref.getValue();
				}
			}
			
			throw new IllegalArgumentException( "Expected ChildReference." );
		}
		
		@Override
		public IndexedRecord toRecord(IndexedRecord value, Schema schema, LogicalType type) {
			LOGGER.debug( "Converting to record." );
			IdentityHashMap<Object,Long> idMap = ReferenceScope.ID_BY_ENTITY.get();
			
			Long id = idMap.get( value );
			
			if ( id == null ) {
				id = (long) idMap.size();
				LOGGER.debug( "Writing {} for the first time with ID {}.", value, id );
				idMap.put( value, id );
				
				return getObject( schema, id, value );
			}
			else {
				LOGGER.debug( "Writing {} again, reusing ID {}.", value, id );
				return getObject( schema, id, null );
			}
		}
		
		private IndexedRecord getObject( Schema schema, long id, IndexedRecord value ) {
		  GenericData.Record record = new GenericData.Record( schema );
		  record.put("id", id);
		  if ( value != null ) {
		    record.put("value", value);
		  }
		  return record;
		}
		
	}
	
	
	public static class Reference extends LogicalType {

		public Reference() {
			super("reference");
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		setup();
		
		Child child1 = Child.newBuilder()
				.setName( "Annika" )
				.build();
		
		Parent parent = Parent.newBuilder()
				.setChild1( child1 )
				.setChild2( child1 )
				.build();
			
		byte[] bytes = write( parent );
		Parent newParent = read( bytes );
		
		bytes = write( parent );

		System.out.println( newParent );
		
		System.out.println( newParent.getChild1() == newParent.getChild2() );
		// TODO Auto-generated method stub

	}

	public static void setup() {
		LogicalTypes.register( "reference", new LogicalTypeFactory() {
			
			@Override
			public LogicalType fromSchema(Schema schema) {
				return new Reference();
			}
		});
		
		LogicalTypes.register( "referenceScope", new LogicalTypeFactory() {
			
			@Override
			public LogicalType fromSchema(Schema schema) {
				return new ReferenceScope();
			}
		});
		
		SpecificData.get().addLogicalTypeConversion( new ReferenceScopeConversion() );
	}
	
	
	private static byte[] write( Parent parent ) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		JsonEncoder encoder = EncoderFactory.get().jsonEncoder( parent.getSchema(), output, true );
		new SpecificDatumWriter<Parent>( parent.getSchema() ).write( parent, encoder );
		encoder.flush();
		return output.toByteArray();
	}
	
	private static Parent read( byte[] input ) throws IOException {
		JsonDecoder decoder = DecoderFactory.get().jsonDecoder( Parent.getClassSchema(),  new ByteArrayInputStream( input ) );
		return new SpecificDatumReader<Parent>( Parent.getClassSchema() ).read( null, decoder );

	}
}
