/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package logical.test;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class Parent extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 2254216555241903933L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Parent\",\"namespace\":\"logical.test\",\"fields\":[{\"name\":\"child1\",\"type\":{\"type\":\"record\",\"name\":\"ChildReference\",\"fields\":[{\"name\":\"id\",\"type\":\"long\"},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"Child\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"}]}]}],\"logicalType\":\"reference\",\"logicalTarget\":\"logical.test.Child\"}},{\"name\":\"child2\",\"type\":\"ChildReference\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<Parent> ENCODER =
      new BinaryMessageEncoder<Parent>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<Parent> DECODER =
      new BinaryMessageDecoder<Parent>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<Parent> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<Parent> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<Parent>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this Parent to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a Parent from a ByteBuffer. */
  public static Parent fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public logical.test.Child child1;
  @Deprecated public logical.test.Child child2;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public Parent() {}

  /**
   * All-args constructor.
   * @param child1 The new value for child1
   * @param child2 The new value for child2
   */
  public Parent(logical.test.Child child1, logical.test.Child child2) {
    this.child1 = child1;
    this.child2 = child2;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return child1;
    case 1: return child2;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  protected static final org.apache.avro.data.TimeConversions.DateConversion DATE_CONVERSION = new org.apache.avro.data.TimeConversions.DateConversion();
  protected static final org.apache.avro.data.TimeConversions.TimeConversion TIME_CONVERSION = new org.apache.avro.data.TimeConversions.TimeConversion();
  protected static final org.apache.avro.data.TimeConversions.TimestampConversion TIMESTAMP_CONVERSION = new org.apache.avro.data.TimeConversions.TimestampConversion();
  protected static final org.apache.avro.Conversions.DecimalConversion DECIMAL_CONVERSION = new org.apache.avro.Conversions.DecimalConversion();
  protected static final logical.ReadAndWriteTest.ReferenceConversion REFERENCE_CONVERSION = new logical.ReadAndWriteTest.ReferenceConversion();

  private static final org.apache.avro.Conversion<?>[] conversions =
      new org.apache.avro.Conversion<?>[] {
      REFERENCE_CONVERSION,
      REFERENCE_CONVERSION,
      null
  };

  @Override
  public org.apache.avro.Conversion<?> getConversion(int field) {
    return conversions[field];
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: child1 = (logical.test.Child)value$; break;
    case 1: child2 = (logical.test.Child)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'child1' field.
   * @return The value of the 'child1' field.
   */
  public logical.test.Child getChild1() {
    return child1;
  }

  /**
   * Sets the value of the 'child1' field.
   * @param value the value to set.
   */
  public void setChild1(logical.test.Child value) {
    this.child1 = value;
  }

  /**
   * Gets the value of the 'child2' field.
   * @return The value of the 'child2' field.
   */
  public logical.test.Child getChild2() {
    return child2;
  }

  /**
   * Sets the value of the 'child2' field.
   * @param value the value to set.
   */
  public void setChild2(logical.test.Child value) {
    this.child2 = value;
  }

  /**
   * Creates a new Parent RecordBuilder.
   * @return A new Parent RecordBuilder
   */
  public static logical.test.Parent.Builder newBuilder() {
    return new logical.test.Parent.Builder();
  }

  /**
   * Creates a new Parent RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new Parent RecordBuilder
   */
  public static logical.test.Parent.Builder newBuilder(logical.test.Parent.Builder other) {
    return new logical.test.Parent.Builder(other);
  }

  /**
   * Creates a new Parent RecordBuilder by copying an existing Parent instance.
   * @param other The existing instance to copy.
   * @return A new Parent RecordBuilder
   */
  public static logical.test.Parent.Builder newBuilder(logical.test.Parent other) {
    return new logical.test.Parent.Builder(other);
  }

  /**
   * RecordBuilder for Parent instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Parent>
    implements org.apache.avro.data.RecordBuilder<Parent> {

    private logical.test.Child child1;
    private logical.test.Child.Builder child1Builder;
    private logical.test.Child child2;
    private logical.test.Child.Builder child2Builder;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(logical.test.Parent.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.child1)) {
        this.child1 = data().deepCopy(fields()[0].schema(), other.child1);
        fieldSetFlags()[0] = true;
      }
      if (other.hasChild1Builder()) {
        this.child1Builder = logical.test.Child.newBuilder(other.getChild1Builder());
      }
      if (isValidValue(fields()[1], other.child2)) {
        this.child2 = data().deepCopy(fields()[1].schema(), other.child2);
        fieldSetFlags()[1] = true;
      }
      if (other.hasChild2Builder()) {
        this.child2Builder = logical.test.Child.newBuilder(other.getChild2Builder());
      }
    }

    /**
     * Creates a Builder by copying an existing Parent instance
     * @param other The existing instance to copy.
     */
    private Builder(logical.test.Parent other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.child1)) {
        this.child1 = data().deepCopy(fields()[0].schema(), other.child1);
        fieldSetFlags()[0] = true;
      }
      this.child1Builder = null;
      if (isValidValue(fields()[1], other.child2)) {
        this.child2 = data().deepCopy(fields()[1].schema(), other.child2);
        fieldSetFlags()[1] = true;
      }
      this.child2Builder = null;
    }

    /**
      * Gets the value of the 'child1' field.
      * @return The value.
      */
    public logical.test.Child getChild1() {
      return child1;
    }

    /**
      * Sets the value of the 'child1' field.
      * @param value The value of 'child1'.
      * @return This builder.
      */
    public logical.test.Parent.Builder setChild1(logical.test.Child value) {
      validate(fields()[0], value);
      this.child1Builder = null;
      this.child1 = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'child1' field has been set.
      * @return True if the 'child1' field has been set, false otherwise.
      */
    public boolean hasChild1() {
      return fieldSetFlags()[0];
    }

    /**
     * Gets the Builder instance for the 'child1' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public logical.test.Child.Builder getChild1Builder() {
      if (child1Builder == null) {
        if (hasChild1()) {
          setChild1Builder(logical.test.Child.newBuilder(child1));
        } else {
          setChild1Builder(logical.test.Child.newBuilder());
        }
      }
      return child1Builder;
    }

    /**
     * Sets the Builder instance for the 'child1' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public logical.test.Parent.Builder setChild1Builder(logical.test.Child.Builder value) {
      clearChild1();
      child1Builder = value;
      return this;
    }

    /**
     * Checks whether the 'child1' field has an active Builder instance
     * @return True if the 'child1' field has an active Builder instance
     */
    public boolean hasChild1Builder() {
      return child1Builder != null;
    }

    /**
      * Clears the value of the 'child1' field.
      * @return This builder.
      */
    public logical.test.Parent.Builder clearChild1() {
      child1 = null;
      child1Builder = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'child2' field.
      * @return The value.
      */
    public logical.test.Child getChild2() {
      return child2;
    }

    /**
      * Sets the value of the 'child2' field.
      * @param value The value of 'child2'.
      * @return This builder.
      */
    public logical.test.Parent.Builder setChild2(logical.test.Child value) {
      validate(fields()[1], value);
      this.child2Builder = null;
      this.child2 = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'child2' field has been set.
      * @return True if the 'child2' field has been set, false otherwise.
      */
    public boolean hasChild2() {
      return fieldSetFlags()[1];
    }

    /**
     * Gets the Builder instance for the 'child2' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public logical.test.Child.Builder getChild2Builder() {
      if (child2Builder == null) {
        if (hasChild2()) {
          setChild2Builder(logical.test.Child.newBuilder(child2));
        } else {
          setChild2Builder(logical.test.Child.newBuilder());
        }
      }
      return child2Builder;
    }

    /**
     * Sets the Builder instance for the 'child2' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public logical.test.Parent.Builder setChild2Builder(logical.test.Child.Builder value) {
      clearChild2();
      child2Builder = value;
      return this;
    }

    /**
     * Checks whether the 'child2' field has an active Builder instance
     * @return True if the 'child2' field has an active Builder instance
     */
    public boolean hasChild2Builder() {
      return child2Builder != null;
    }

    /**
      * Clears the value of the 'child2' field.
      * @return This builder.
      */
    public logical.test.Parent.Builder clearChild2() {
      child2 = null;
      child2Builder = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Parent build() {
      try {
        Parent record = new Parent();
        if (child1Builder != null) {
          record.child1 = this.child1Builder.build();
        } else {
          record.child1 = fieldSetFlags()[0] ? this.child1 : (logical.test.Child) defaultValue(fields()[0], record.getConversion(0));
        }
        if (child2Builder != null) {
          record.child2 = this.child2Builder.build();
        } else {
          record.child2 = fieldSetFlags()[1] ? this.child2 : (logical.test.Child) defaultValue(fields()[1], record.getConversion(1));
        }
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<Parent>
    WRITER$ = (org.apache.avro.io.DatumWriter<Parent>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<Parent>
    READER$ = (org.apache.avro.io.DatumReader<Parent>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
