package logical;

import java.io.File;
import org.apache.avro.LogicalType;
import org.apache.avro.Protocol;
import org.apache.avro.Schema;
import org.apache.avro.compiler.specific.SpecificCompiler;

public class CustomCompiler extends SpecificCompiler {

	public CustomCompiler(Protocol protocol) {
		super(protocol);
	}

	public CustomCompiler( Schema schema ) {
		super( schema );
	}

	@Override
	public String javaType(Schema schema) {
		Object classProp = schema.getObjectProp( "logicalTarget" );
		if ( classProp != null ) {
			return classProp.toString();
		}
		else {
			return super.javaType(schema);
		}
	}
	
	@Override
	public String javaUnbox(Schema schema) {
		Object classProp = schema.getObjectProp( "logicalTarget" );
		if ( classProp != null ) {
			return classProp.toString();
		}
		else {
			return super.javaUnbox(schema);
		}
	}
	
	
 
	@Override
  public String conversionInstance(Schema schema) {
	  if ( schema != null ) {
	    LogicalType type = schema.getLogicalType();
	    if ( type instanceof ReadAndWriteTest.Reference ) {
	      return "REFERENCE_CONVERSION";
	    }
	  }

	  return super.conversionInstance(schema);
  }

  public static void main( String[] args ) throws Exception {
		File input = new File( args[ 0 ] );
		File destination = new File( args[ 1 ] );
	
		ReadAndWriteTest.setup();

		Schema schema = new Schema.Parser().parse( input );
		
		
		new CustomCompiler( schema ).compileToDestination( input, destination );
	}
}
