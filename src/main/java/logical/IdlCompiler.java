package logical;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.avro.Schema;
import org.apache.avro.compiler.idl.Idl;

public class IdlCompiler {

	public static void main(String[] args) throws Exception {
		Idl parser = new Idl(new File(args[0]));
		File outputDirectory = new File(args[1]);

		for ( Schema schema : parser.CompilationUnit().getTypes() ) {
			print(schema, outputDirectory, true);
		}
		parser.close();

	}

	private static void print(Schema protocol, File outputDirectory, boolean pretty) throws FileNotFoundException {
		FileOutputStream fileOutputStream = new FileOutputStream( new File( outputDirectory, protocol.getName() + ".schema"));
		PrintStream printStream = new PrintStream(fileOutputStream);
		printStream.println(protocol.toString(pretty));
		printStream.close();
	}

}
